export default {
  it: 'Italiano',
  en: 'English',
  es: 'Español',
  ca: 'Català',
  pl: 'Polski',
  eu: 'Euskara'
}
